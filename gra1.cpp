// gra1.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <vector>
#include <ctime>
#include <string>
using namespace std;

class karty
{
private:
	int wartosc;
	string nazwa;
	string kolor;
public:
	karty(int v, string n, string c) :wartosc(v), nazwa(n), kolor(c) {}
	int getWartosc()
	{
		return wartosc;
	}
	string getNazwa()
	{
		return nazwa;
	}
	string getKolor()
	{
		return kolor;
	}
	friend ostream& operator<<(ostream& strumien, karty c)
	{
		strumien<<c.getNazwa() << " " << c.getKolor();
		return strumien;
	}
};

class Talia
{
private:
	vector<Karta> talia;
public:
	void add(Karta c) 
	{
		talia.push_back(c);
	}
	Karta popKarta() 
	{
		srand(time(0));
		int index = rand() % talia.size();
		Card tmp = talia[index];
		talia[index] = talia.back();
		talia.pop_back();
		return tmp;
	}
int main()
{
	
return 0;
}

